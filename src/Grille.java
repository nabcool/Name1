import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Grille {
    private int hauteur;
    private int largeur;
    private int maxCaseH;
    private int maxCaseL;
    private static int sizecase=30;
    private ArrayList<Case> cases;
    private ArrayList<Case> caseOccuper;
    /*
        X correspond a la largeur.
        Y correspond a la hauteur.
     */

    public Grille(int y, int x) {
        this.hauteur = y;
        this.largeur = x;
        this.maxCaseH=hauteur/sizecase;
        this.maxCaseL=largeur/sizecase;
        System.out.println(maxCaseH);
        System.out.println(maxCaseL);
        cases=new ArrayList<Case>();
        caseOccuper=new ArrayList<Case>();
    }
    /*
        i1 et i =0
        Case(9,39,32,64)
        i=1
        Case(9,39,30,60)
     */
    public void setGrille() {
        int y1=0;
        int x1=0;
        int y2=30;
        int x2=30;
        for (int i1 = 0; i1 < maxCaseH-1; i1++) {
            if(i1!=0){
                x1= i1*sizecase;
                x2 = (i1+1) * sizecase;
            }
            for (int i = 0; i < maxCaseL; i++) {
                if (i != 0) {
                    y1=i*sizecase;
                    y2 = (i+1) * sizecase;
                }
               // Random rand=new Random();
                //int randomNum = rand.nextInt((1 - 0) + 1) + 0;
                int randomNum=0;
                if(cases.size()==159 || cases.size()==160 || cases.size()==185 || cases.size()==186 ||
                        cases.size()==211 || cases.size()==212 || cases.size()==162 ||
                        cases.size()==188 || cases.size()==214){
                    System.out.println("ICI");
                    randomNum=1;
                }
                else if(i==1 || i+4==maxCaseL || i1==4 || i1==maxCaseH-10){
                    randomNum=1;
                }
                //test a enlever
                else{
                    randomNum=0;
                }
                Case c=new Case(x1,x2,y1,y2,randomNum);
                cases.add(c);
                if(randomNum==1){
                    caseOccuper.add(c);
                }
            }
            y1=0;
            y2=0;
        }
        System.out.println(cases.get(159));
        System.out.println(cases.size());
    }
    public Case getCasePacman(){
        int total=0;
        Pacman pc=main.f.getControl().getPacman();
        int i=pc.getPosX()/sizecase;
        total+=i*maxCaseL;
        int i2=pc.getPosY()/sizecase;
        total+=i2;
        return cases.get(total);
    }
    public Case getCaseDroite(Case c){
        int total=0;
        int i=c.getPosX()/sizecase;
        total+=i*maxCaseL;
        int i2=c.getPosY()/sizecase;
        total+=i2;
        total+=maxCaseL;
        return cases.get(total);
    }
    public Case getCaseGauche(Case c){
        int total=0;
        int i=c.getPosX()/sizecase;
        total+=i*maxCaseL;
        int i2=c.getPosY()/sizecase;
        total+=i2;
        total-=maxCaseL;
//        System.out.println("===debug===");
//        System.out.println(total);
//        System.out.println(cases.get(total));
//        System.out.println(getCasePacman());
//        System.out.println("===fin===");
        if(total<0){
            total=0;
        }
        return cases.get(total);
    }
    public Case getCaseBas(Case c){
        int total=0;
        int i=c.getPosX()/sizecase;
        total+=i*maxCaseL;
        int i2=c.getPosY()/sizecase;
        total+=i2;
        total+=1;
        return cases.get(total);
    }
    public Case getCaseHaut(Case c){
        int total=0;
        int i=c.getPosX()/sizecase;
        total+=i*maxCaseL;
        int i2=c.getPosY()/sizecase;
        total+=i2;
        total-=1;
        if(total<0){
            total=0;
        }
        return cases.get(total);
    }
    public Pacman getPacman(){
        return main.f.getControl().getPacman();
    }
    public boolean DeplacementDroiteOK(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossible()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheOK(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossible()){
            System.out.println(this.getCaseGauche(pacman).DeplacementPossible());
            return true;
        }
        else if((this.getPacman().getPosX())-15>=this.getCasePacman().getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautOK(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossible()){
            return true;
        }
        else if((this.getPacman().getPosY())-15>this.getCasePacman().getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasOK(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossible()){
            return true;
        }
        return false;
    }
    public ArrayList<Case> getCaseOccuper(){
        return this.caseOccuper;
    }
}