import com.thoughtworks.xstream.mapper.Mapper;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.Exception;

public class Fenetre extends javax.swing.JFrame{
    public BufferedImage image;
    public BufferedImage image2;
    public BufferedImage background;
    boolean start=false;
    boolean swap=false;
    boolean pause=false;
    JPanel paneprincpal;
    JPanel paneGrille;
    JLabel labPacman;
    Container cont;
    Controleur c;
    ImageIcon pacman;
    // private Image photo1 = getToolkit().getImage("pacman1.png");
    public Fenetre() {
        this.setTitle("PACMAN");
        this.setSize(1200, 900);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.RED);

        cont=this.getContentPane();
        cont.setLayout(null);
        cont.setBounds(0,0,1200,800);
        try {
            image = ImageIO.read(new File("image/pacman1.png"));
            image2 = ImageIO.read(new File("image/pacman2.png"));
            background = ImageIO.read(new File("image/pacman_layout.JPG"));
        }
        catch(Exception e){
            e.printStackTrace();
        }
        pacman = new ImageIcon("image/pacman1.png");
        labPacman= new JLabel(pacman);
        c=new Controleur();
        labPacman.setBounds(c.getPacman().getPosX(),c.getPacman().getPosY(),30,30);

        ImageIcon backgroundRaw = new ImageIcon("image/pacman_layout.JPG");
        ImageIcon background = new ImageIcon(new ImageIcon("image/pacman_layout.JPG").getImage().getScaledInstance(1100, 750, Image.SCALE_SMOOTH));
        JLabel labBackground= new JLabel(background);
        labBackground.setBounds(0,0,1100,750);

        paneprincpal=new JPanel();
        paneprincpal.setLayout(null);
        paneprincpal.setBounds(0,0,1200,800);
        paneprincpal.setPreferredSize(new Dimension(1200,800));
        paneprincpal.setOpaque(false);
        paneprincpal.setDoubleBuffered(true);

        paneGrille=new JPanel();
        paneGrille.setLayout(null);
        paneGrille.setBounds(0,0,1200,800);
        paneGrille.setPreferredSize(new Dimension(1200,800));
        paneGrille.setOpaque(false);
        paneGrille.setDoubleBuffered(true);


        this.addKeyListener(c);

        paneGrille.add(labPacman);
        paneprincpal.add(paneGrille);
        paneprincpal.add(labBackground);
        cont.add(paneprincpal);



        this.setVisible(true);
       //this.paintComponents(paneprincpal.getGraphics());

    }
//    public void paintComponents(Graphics g) {
//        super.paintComponents(g);
//        try {
//            image = ImageIO.read(new File("image/pacman1.png"));
//            image2 = ImageIO.read(new File("image/pacman2.png"));
//            background = ImageIO.read(new File("image/pacman_layout.JPG"));
//        }
//        catch(Exception e){
//            e.printStackTrace();
//        }
//        //g.drawRect(c.getPacman().getPosX(),c.getPacman().getPosY(),30,30);
//       // g.drawRect(1140,30,30,30);
//        Graphics g2=paneGrille.getGraphics();
//        g2.drawImage(image, c.getPacman().getPosX(), c.getPacman().getPosY(), 30 , 30, null);
//        g.drawImage(background,30,30,1200,800,null);
//
//
//    }
    public Controleur getControl(){
        return this.c;
    }
}
