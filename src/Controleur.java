import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Controleur implements ActionListener,KeyListener{
    Pacman pc;
    Grille gr;
    public Controleur(){
        pc=new Pacman(510,516,"Pacman");
        gr=new Grille(1200,800);
        gr.setGrille();
    }
    public Pacman getPacman(){
        return this.pc;
    }
    public Grille getGrille(){
        return this.gr;
    }
    public void actionPerformed(ActionEvent e) {

    }
    public void keyPressed(KeyEvent e) {
        if(main.f.start==true){
            if (e.getKeyCode() == KeyEvent.VK_RIGHT){
                pc.setDirection(2);
            }
            if (e.getKeyCode() == KeyEvent.VK_LEFT){
                pc.setDirection(4);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP){
                pc.setDirection(1);
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN){
                pc.setDirection(3);
            }
        }
        if(e.getKeyCode() == KeyEvent.VK_P) {
            main.f.start=false;
            for(Case x : gr.getCaseOccuper()){
                System.out.println(x);
            }
        }
        // Invoked when a key has been pressed.
        if(e.getKeyCode() == KeyEvent.VK_S){
            main.f.start=true;
            new Thread()
            {
              public void run() 
              {
                  /* Creer une classe Jeu ??? */
                while(pc.getMort()==false && main.f.start==true ){
                    //f.paint(g);
                    try{
                        this.sleep(190);
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
//                    System.out.println("=========");
//                    System.out.println(gr.getCaseBas(gr.getCasePacman()));
//                    System.out.println("=========");
                    if( (pc.getDirection()==0 || pc.getDirection()==2)){
                        if(gr.DeplacementDroiteOK(gr.getCasePacman())) {
                            pc.incrementePosX();
                        }

                    }
                    if(pc.getDirection()==3){
                        if(gr.DeplacementBasOK(gr.getCasePacman())) {
                            pc.incrementePosY();
                        }
                    }
                    if(pc.getDirection()==1){
                        if(gr.DeplacementHautOK(gr.getCasePacman())) {
                            pc.decrementePosY();
                        }
                    }
                    if(pc.getDirection()==4){
                        if(gr.DeplacementGaucheOK(gr.getCasePacman())) {
                            pc.decrementePosX();
                        }
                    }
                    System.out.println(pc);
                    System.out.println(gr.getCasePacman());
                    Graphics g=main.f.paneGrille.getGraphics();
                    if(main.f.swap==false){
                       // main.f.paintComponents(g);
                       // main.f.paneGrille.repaint(25,pc.getPosX(),pc.getPosY(),30,30);
                     //   main.f.paneGrille.repaint(25,pc.getPosX()-15,pc.getPosY()-15,30,30);
                       // main.f.paneprincpal.update(g);
                        main.f.labPacman.setBounds(pc.getPosX(),pc.getPosY(),30,30);
                        main.f.pacman.setImage(main.f.image);
                      // g.drawImage(main.f.image, pc.getPosX(), pc.getPosY(), 30 , 30, null);
                       // g.drawRect(pc.getPosX(),pc.getPosY(),30,30);
                        main.f.swap=true;
                    }
                    else if(main.f.swap==true){
                       // main.f.paneGrille.repaint(25,pc.getPosX(),pc.getPosY(),30,30);
                        //main.f.paneGrille.repaint(25,pc.getPosX()-15,pc.getPosY()-15,30,30);
                       // main.f.paneprincpal.update(g);
                        main.f.labPacman.setBounds(pc.getPosX(),pc.getPosY(),30,30);
                        main.f.pacman.setImage(main.f.image2);
                        //g.drawImage(main.f.image2, pc.getPosX(), pc.getPosY(), 30 , 30, null);
                        //g.drawRect(pc.getPosX(),pc.getPosY(),30,30);
                        main.f.swap=false;
                    }


                    /*else if{
                        this.stop();
                    }*/
                }
            }
            }.start();
        }
        
    }
    public void keyReleased(KeyEvent e) {
    }
    public void keyTyped(KeyEvent e) {
        // Invoked when a key has been typed.
    }
}
//Pour start le jeu
/*Graphics g=f.getGraphics();
            new Thread()
            {
              public void run() 
              {
                while(f.mort==false){
                    //f.paint(g);
                    try{
                        this.sleep(250);
                        System.out.println("Attente fini");
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                    if(f.swap==false){
                        f.posX1+=13;
                        f.paint(g);
                        g.drawImage(f.image, f.posX1, f.posY1, 30 , 30, null);
                        f.swap=true;
                    }
                    else{
                        f.posX1+=13;
                        f.paint(g);
                        g.drawImage(f.image2, f.posX1, f.posY1, 30 , 30, null);
                        f.swap=false;
                    }
                }
            }
            }.start();*/