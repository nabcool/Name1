/**
 * Created by willi on 04/02/2016.
 */
public class Case {
   // private static int sizeH=30;
   // private static int sizeY=30;
    private int posX1,posX2,posY1,posY2;
    private int type;
    /*
        type :
            0 = Libre
            1 = Cases mur
            2 = Base fantomes
            3 = Fantomes
     */
    private boolean libre;

    public Case(int a,int b, int c, int d,int type){
        this.posX1=a;
        this.posX2=b;
        this.posY1=c;
        this.posY2=d;
        this.type=type;
        if(this.type!=0){
            this.libre=false;
        }
        else{
            this.libre=true;
        }
    }
    public int getType(){
        return this.type;
    }
    // bizarre marche pour droite et bas //
    public boolean DeplacementPossible(){
        if(this.type==0 || this.type==3){
            return true;
        }
        else{
            return false;
        }
    }
    public Pacman getPacman(){
        return main.f.getControl().getPacman();
    }

    public String toString(){
        return "Case X : "+posX1+","+posX2+" Case Y : "+posY1+","+posY2+".";
    }
    public int getPosX(){
        return this.posX1;
    }
    public int getPosX2(){
        return this.posX2;
    }
    public int getPosY(){
        return this.posY1;
    }
}
