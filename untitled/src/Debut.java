

import javax.swing.*;
import java.awt.*;

public class Debut extends JFrame {

    Container cont;
    Controleur c;
    MyButton joueurunjoueur;

    public Debut() {
        this.setTitle("PACMAN");
        this.setSize(630, 730);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.RED);

        cont = this.getContentPane();

        JPanel panel= new JPanel();

        panel.setLayout (new BoxLayout( panel , BoxLayout.Y_AXIS ) ) ;

     //   ImageIcon backgroundRaw = new ImageIcon("image/pacman_layout.JPG");
        ImageIcon background = new ImageIcon(new ImageIcon("image/Accueil/imageacceuil.png").getImage());
        JLabel labBackground= new JLabel(background);
      //  labBackground.setBounds(0, 0, 1100, 750);

        panel.add(labBackground);

        joueurunjoueur = new MyButton("Jouer 1 joueur","image/btn.png","");
        MyButton joueurdeuxjoueur = new MyButton("Jouer 2 joueur","image/btn.png","imge/fondblanc.png");
        MyButton aide = new MyButton("Aide","image/btn.png","imae/fondblanc.png");
        MyButton meilleurscore = new MyButton("Meilleur score","image/btn.png","image/ondblanc.png");

        JPanel joueurunjoueurpanel= new JPanel();
        JPanel joueurdeuxjoueurpanel= new JPanel();
        JPanel aidepanel= new JPanel();
        JPanel meilleurscorepanel= new JPanel();


        joueurunjoueur.setPreferredSize(new Dimension(200, 40));
        joueurdeuxjoueur.setPreferredSize(new Dimension(200, 40));
        aide.setPreferredSize(new Dimension(200,40));
        meilleurscore.setPreferredSize(new Dimension(200, 40));

        joueurunjoueurpanel.setBackground( Color.black );
        joueurdeuxjoueurpanel.setBackground( Color.black );
        aidepanel.setBackground( Color.black );
        meilleurscorepanel.setBackground( Color.black );


        //~ /* ActionBouton o = new ActionBouton (fenetre,this,null,null,null,null,null); */
        ControleurDebut o = new ControleurDebut(this,null);

        joueurunjoueur.setActionCommand("lienjouerunjoueur");
        joueurunjoueur.addActionListener(o);


        joueurunjoueurpanel.add(joueurunjoueur);
        joueurdeuxjoueurpanel.add(joueurdeuxjoueur);
        aidepanel.add(aide);
        meilleurscorepanel.add(meilleurscore);


        panel.add(joueurunjoueurpanel);
        panel.add(joueurdeuxjoueurpanel);
        panel.add(aidepanel);
        panel.add(meilleurscorepanel);

        panel.setBackground(Color.black);

        cont.add(panel);
        cont.setBackground(Color.black);
        this.setVisible(true);

    }

}
