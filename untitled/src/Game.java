

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by willi on 03/03/2016.
 */
public class Game {
    Map<String,Clip> musique;
    String nom;
    private boolean start=false;

    public Game(String s){
        this.nom = s;
        musique = new HashMap<String,Clip>();

    }

    public void addMusique(String chemin, String titre) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        URL url = this.getClass().getClassLoader().getResource(chemin);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
        // Get a sound clip resource.
        Clip c;
        c = AudioSystem.getClip();
        c.open(audioIn);
        musique.put(titre,c);
    }
    public void PlayMusique(String titre){
        musique.get(titre).start();
    }
    public void StopMusisque(String titre){
        musique.get(titre).stop();
    }
    public boolean getStart(){
        return this.start;
    }
    public void setStart(boolean b){
        this.start=b;
    }
}
