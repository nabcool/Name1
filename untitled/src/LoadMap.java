
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by willi on 24/02/2016.
 */
public class LoadMap {
    private static int sizecase = 30;

    public LoadMap(Grille g) {
        g.redefineGrille(570, 660);
        //System.out.println("Hauteur :"+g.hauteur);
        //System.out.println("Largeur :"+g.largeur);
        //System.out.println("MaxCaseH :"+g.maxCaseH);
        //System.out.println("MaxCaseL :"+g.maxCaseL);
        FileReader fr = null;
        int cpt = 0;
        try {
            File f = new File("Map.txt");
            fr = new FileReader(f);
        } catch (FileNotFoundException exception) {
            System.out.println("Le fichier n'a pas été trouvé");
        }
        int c=0;
        int y1=0;
        int x1=0;
        int y2=30;
        int x2=30;
        while (c != -1) {
       //     System.out.print((char) c);
            try {
                c = fr.read();
            } catch (IOException exception) {
                System.out.println("Erreur lors de la lecture : " + exception.getMessage());
            }

            if(c == 120){
              //  System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,1);
               // System.out.println(cas);
                g.getCaseOccuper().add(cas);
                g.getCase().add(cas);
                x1+=sizecase;
                x2+=sizecase;
            }
            else if(c == 121){
               // System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,0);
                g.getCase().add(cas);
                g.getCaseLibre().add(cas);
                x1+=sizecase;
                x2+=sizecase;
            }
            else if(c == 119){
                //  System.out.print("Coucou"+(char)c);
                Case cas=new Case(x1,x2,y1,y2,2);
                g.getCase().add(cas);
                g.getCaseLibre().add(cas);
                x1+=sizecase;
                x2+=sizecase;

            }
            else if(c == 118){
                  System.out.print("Coucou"+(char)c);
                  Case cas=new Case(x1,x2,y1,y2,5);
                  g.getCase().add(cas);
                  g.casebouleinvincible.add(cas);
                  x1+=sizecase;
                 x2+=sizecase;

               }
            else{

            }

            if(x2>g.hauteur){
                x1=0;
                x2=30;
                y1+=sizecase;
                y2+=sizecase;
            }
        }
    }
}

