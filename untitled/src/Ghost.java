/**
 * Created by aurelien on 01/03/16.
 */
public class Ghost{


    int memoirecheminverse;
private int posX;
private int posY;
private String nom;
private String image[];
private boolean mort;
private int direction;
private boolean disableDirection;
int memoire;

        /*
            Y correspond a la largeur.
            X correspond a la hauteur.
         */
        public Ghost(int p1,int p2, String nom){
            this.posX=p1;
            this.posY=p2;
            this.nom=nom;
            this.mort=false;
            this.direction=0;
            this.memoire=0;
            this.disableDirection=false;
        }
        public boolean getMort(){
            return this.mort;
        }
        public void Mort(){
            this.mort= true;
        }
        public int getDirection(){
            return this.direction;
        }
        public void setDirection(int d){
            this.direction=d;
        }
        public void incrementePosX(){
            posX+=15;
        }
        public void incrementePosY(){
            posY+=15;
        }
        public void decrementePosY(){
            posY-=15;
        }
        public void decrementePosX(){
            posX-=15;
        }
        public void setPos(int x, int y){
            this.posX=x;
            this.posY=y;
        }
        public int getPosX(){
            return this.posX;
        }
        public int getPosY(){
            return this.posY;
        }
        public boolean getDisableDir(){
            return this.disableDirection;
        }
        public void setDisableDirection(boolean b){
            this.disableDirection=b;
        }
        public String toString(){
            return "Fantome X : "+getPosX()+" Y : "+getPosY()+".";
        }

}
