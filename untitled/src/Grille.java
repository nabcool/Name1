
import java.util.ArrayList;


public class Grille {
    int hauteur;
    int largeur;
    int maxCaseH;
    int maxCaseL;
    private final static int sizecase=30;
    private ArrayList<Case> cases;
    private ArrayList<Case> caseOccuper;

    private ArrayList<Case> caseLibre;

    ArrayList<Case> caseLibreNonboulejaune;

    ArrayList<Case> casebouleinvincible;
    ArrayList<Case> casecerise;

    /*
        X correspond a la largeur.
        Y correspond a la hauteur.
     */
    /*
        Construit une grille définie par une hauteur, une largeur, la hauteurMax d'une case,
        la largeurMax calculé par la taille globale de la case.
        La grille possede differents tableaux de case.
    */
    public Grille(int y, int x) {
        this.hauteur = y;
        this.largeur = x;
        this.maxCaseH=hauteur/sizecase;
        this.maxCaseL=largeur/sizecase;


       // System.out.println(maxCaseH);
       // System.out.println(maxCaseL);
        casecerise=new ArrayList<Case>();

        cases=new ArrayList<Case>();
        caseOccuper=new ArrayList<Case>();
        caseLibre=new ArrayList<Case>();
        caseLibreNonboulejaune=new ArrayList<Case>();
        casebouleinvincible=new ArrayList<Case>();

    }
    /*
     Permet de redefinir une grille apres un LoadMap par exemple.
    */
    public void redefineGrille(int x, int y){
        this.hauteur = x;
        this.largeur = y;
        this.maxCaseH=hauteur/sizecase;
        this.maxCaseL=largeur/sizecase;

    }
    /*
        La grille est chargé par la classe LoadMap.
     */
    public void setGrille(){
        new LoadMap(this);
    }


    /*
        Ancienne methode qui permetait de placer une grille par dessus une image
        en fond d'ecran.
     */
//    public void setGrille2() {
//        int y1=0;
//        int x1=0;
//        int y2=30;
//        int x2=30;
//        for (int i1 = 0; i1 < maxCaseH-1; i1++) {
//            if(i1!=0){
//                x1= i1*sizecase;
//                x2 = (i1+1) * sizecase;
//            }
//            for (int i = 0; i < maxCaseL; i++) {
//                if (i != 0) {
//                    y1=i*sizecase;
//                    y2 = (i+1) * sizecase;
//                }
//               // Random rand=new Random();
//                //int randomNum = rand.nextInt((1 - 0) + 1) + 0;
//                int randomNum=0;
//                if(cases.size()==159 || cases.size()==160 || cases.size()==185 || cases.size()==186 ||
//                        cases.size()==211 || cases.size()==212 || cases.size()==162 ||
//                        cases.size()==188 || cases.size()==214){
//                    //System.out.println("ICI");
//                    randomNum=1;
//                }
//                else if(i==1 || i+4==maxCaseL || i1==4 || i1==maxCaseH-10){
//                    randomNum=1;
//                }
//                //test a enlever
//                else{
//                    randomNum=0;
//                }
//                Case c=new Case(x1,x2,y1,y2,randomNum);
//                cases.add(c);
//                if(randomNum==1){
//                    caseOccuper.add(c);
//                }
//                else{
//                    caseLibre.add(c);
//                }
//            }
//            y1=0;
//            y2=0;
//        }
//    }
    /*
        Permet de connaitre la case sur laquel ce trouve le pacman.
        En cas de doute, sur la case du pacman,le changement de direction du pacman est désactivé.
        Le doute arrive lorsque le pacman est entre deux cases.
     */

    public Case getCasePacman(){
        int total=0;
        Pacman pc=ControleurDebut.f.getControl().getPacman();
        int i=pc.getPosY()/sizecase;
        if(pc.getPosY()%sizecase!=0){
            pc.setDisableDirection(true);
        }
        total+=i*maxCaseH;
        int i2=pc.getPosX()/sizecase;
        if(pc.getPosX()%sizecase!=0){
            pc.setDisableDirection(true);
        }
        total+=i2;
        return cases.get(total);
    }


    public Case getCaseFantome(){
        //  System.out.println("Case Pacman");
        int total=0;
        Ghost fantomeun=ControleurDebut.f.getControl().getGhostun();
        int i=fantomeun.getPosY()/sizecase;
        if(fantomeun.getPosY()%sizecase!=0){
            fantomeun.setDisableDirection(true);
            // pc.memoire=pc.getDirection();
        }
        total+=i*maxCaseH;
        int i2=fantomeun.getPosX()/sizecase;
        if(fantomeun.getPosX()%sizecase!=0){
            fantomeun.setDisableDirection(true);
            //  pc.memoire=pc.getDirection();
        }
        total+=i2;
        return cases.get(total);
    }

    public int getCasePacmanX1(){
        int total=0;
        Pacman pc=ControleurDebut.f.getControl().getPacman();
        int i=pc.getPosX()/sizecase;
        return i;
    }

    public int getCasePacmanY1(){

        Pacman pc=ControleurDebut.f.getControl().getPacman();
        int i2=pc.getPosY()/sizecase;
        return i2;
    }


    /*
        Les differentes fonctions suivantes permettent de connaitre les cases ce trouvant autour d'une
        case passer en parametre.
     */

    public Case getCaseDroite(Case c){
        int total=0;
        //Pacman pc=main.f.getControl().getPacman();
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH+1;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        return cases.get(total);
    }
    public Case getCaseGauche(Case c){
        int total=0;
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH-1;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        if(total<0){
            total=0;
        }
        return cases.get(total);
    }

    public Case getCaseBas(Case c){
        int total=0;
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        total+=maxCaseH;
        return cases.get(total);
    }
    public Case getCaseHaut(Case c){
        int total=0;
        int i=c.getPosY()/sizecase;
        total+=i*maxCaseH;
        int i2=c.getPosX()/sizecase;
        total+=i2;
        total-=maxCaseH;

        //System.out.println("Case de Bas"+cases.get(total));
        // System.out.println("Case de Bas"+cases.get(total).getType());

        return cases.get(total);
    }
    /* ¨
    Raccourci pour acceder au Pacman.
     */
    public Pacman getPacman(){
        return ControleurDebut.f.getControl().getPacman();
    }
    /*
        Les differentes fonctions suivantes permettent de savoir si il est possible de
        se deplacer sur une case adjacentes pour le pacman.
        Afin que cela fonctionne pour les fantomes egalement il suffit de rajouter un parametre : Objet.
     */
    public boolean DeplacementDroiteOK(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossible()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheOK(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossible()){


           // System.out.println(this.getPacman().getPosX());
           // System.out.println(this.getPacman().getPosY());
           // if(this.getCaseGauche(tmp).DeplacementPossible()) {
                //System.out.println("CAS A");
                return true;
      //   }


        }
        else if((this.getPacman().getPosX())-15>=this.getCasePacman().getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautOK(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossible()){
            return true;
        }
        else if((this.getPacman().getPosY())-15>=this.getCasePacman().getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasOK(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossible()){
            return true;
        }
        return false;
    }


    //DEPLACEMENT FANTOME1

    public Ghost getFantome(){
        return ControleurDebut.f.getControl().getGhostun();
    }
    public boolean DeplacementDroiteFantomeOK(Case pacman){
        if(this.getCaseDroite(pacman).DeplacementPossible()){
            return true;
        }
        return false;
    }
    public boolean DeplacementGaucheFantomeOK(Case pacman){
        if(this.getCaseGauche(pacman).DeplacementPossible()){

            // System.out.println(this.getPacman().getPosX());
            // System.out.println(this.getPacman().getPosY());
            // if(this.getCaseGauche(tmp).DeplacementPossible()) {
            //System.out.println("CAS A");
            return true;
            //   }

        }
        else if((this.getFantome().getPosX())-15>=this.getCaseFantome().getPosX()){
            return true;
        }
        return false;
    }
    public boolean DeplacementHautFantomeOK(Case pacman){
        if(this.getCaseHaut(pacman).DeplacementPossible()){
            return true;
        }
        else if((this.getFantome().getPosY())-15>=this.getCaseFantome().getPosY()){
            return true;
        }
        return false;
    }
    public boolean DeplacementBasFantomeOK(Case pacman){
        if(this.getCaseBas(pacman).DeplacementPossible()){
            return true;
        }
        return false;
    }

    //DEPLACEMENT FANTOME1




    /*
        getDirection() : Donne la direction d'ou vient le pacman.
     */

    public int getDirection(Case pacman){
        int compteur=0;
        int resultat= getPacman().getDirection();
        int resSave= getPacman().getDirection();
        int position=20;
        if(resultat==0 || resultat==2){
            position=4;
        }
        else if(resultat==4){
            position=2;
        }
        else if(resultat==1){
            position=3;
        }
        else if(resultat==3){
            position=1;
        }
        if (getCaseDroite(pacman).DeplacementPossible() && position!=2) {
            compteur+=1;
            resultat=2;
        }
        if (getCaseHaut(pacman).DeplacementPossible() && position!=1) {
            compteur+=1;
            resultat=1;
        }
        if (getCaseGauche(pacman).DeplacementPossible() && position!=4) {
            compteur+=1;
            resultat=4;
        }
        if (getCaseBas(pacman).DeplacementPossible() && position!=3) {
            compteur+=1;
            resultat=3;
        }
        //System.out.println(compteur);
        if(compteur==1){
            return resultat;
        }
        else{
            return resSave;
        }
    }
    public ArrayList<Case> getCaseOccuper(){
        return this.caseOccuper;
    }

    public ArrayList<Case> getCase() {
        return this.cases;
    }
    public ArrayList<Case> getCaseLibre(){
        return this.caseLibre;

    }
}