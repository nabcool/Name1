

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Pacman{
    private int posX;
    private int posY;
    private String nom;
    private Map<String,BufferedImage> images;
    private String imagePacman;
    private boolean mort;
    private int direction;
    private boolean disableDirection;
    int memoire;
    /*
        Y correspond a la largeur.
        X correspond a la hauteur.
     */
    public Pacman(int p1,int p2, String nom){
        this.posX=p1;
        this.posY=p2;
        this.nom=nom;
        this.mort=false;
        this.direction=0;
        this.memoire=0;
        this.disableDirection=false;
        this.images=new HashMap<String,BufferedImage>();
        File repertoire = new File("./image/Pacman");
        String[] children = repertoire.list();
        for(int i=0;i<children.length;i++){
           System.out.println(children[i]);
            try {
                BufferedImage image = ImageIO.read(new File("image/Pacman/"+children[i]));
                if(children[i].equals("PacmanDroiteO.png")){
                    this.imagePacman=children[i].substring(0,children[i].length()-4);
                }
                images.put(children[i].substring(0,children[i].length()-4),image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public boolean getMort(){
        return this.mort;
    }
    public void Mort(){
        this.mort= true;
    }
    public int getDirection(){
        return this.direction;
    }
    public void setDirection(int d){
        this.direction=d;
    }
    public void setPos(int x, int y){
        this.posX=x;
        this.posY=y;
    }
    public void incrementePosX(){
        posX+=15;
    }
    public void incrementePosY(){
        posY+=15;
    }
    public void decrementePosY(){
        posY-=15;
    }
    public void decrementePosX(){
        posX-=15;
    }
    public int getPosX(){
        return this.posX;
    }
    public int getPosY(){
        return this.posY;
    }
    public void setPosX(int pos) { this.posX=pos;}
    public void setPosY(int pos) { this.posY=pos;}
    public void setPosPacman(int posX,int posY){
        this.posX=posX;
        this.posY=posY;
    }
    public boolean getDisableDir(){
        return this.disableDirection;
    }
    public void setDisableDirection(boolean b){
        this.disableDirection=b;
    }
    public BufferedImage getImageActuel(){
        return this.images.get(this.imagePacman);
    }
    public void setStringImageActuel(String s){
        this.imagePacman=s;
    }
    public String getStringImageAcutel(){
        return this.imagePacman;
    }
    public String toString(){
        return "Pacman X : "+getPosX()+" Y : "+getPosY()+".";
    }
}